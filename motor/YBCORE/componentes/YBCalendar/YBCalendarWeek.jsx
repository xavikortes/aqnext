var React = require("react");
var YBCalendarDay = require("./YBCalendarDay.jsx");

var YBCalendarWeek = React.createClass({

    _renderCalendarDays: function() {
        var day = [];
        // console.log(this.props.firstWeekDay)
        var firstWeek = true;
        for (var i = this.props.firstWeekDay, l = this.props.firstWeekDay + 7; i < l; i++) {
            var dayData = {};
            if (i in this.props.items) {
                dayData = this.props.items[i];
            }
            var objAtts = {
                "name": "day" + i,
                "YB": this.props.YB,
                "LAYOUT": this.props.LAYOUT,
                "items": dayData,
                "aplic": this.props.aplic,
                "prefix": this.props.prefix,
                "urlStatic": this.props.urlStatic,
                "month": this.props.month,
                "year": this.props.year,
                "totalDays": this.props.totalDays,
                "day": i,
                "firstDay": this.props.firstDay,
                "firstWeek": firstWeek
            };
            var objFuncs = {};
            day.push(YBCalendarDay.generaYBCalendarDay(objAtts, objFuncs));
            firstWeek = false;
        }
        return day;
    },

    render: function() {
        var calendarDays = this._renderCalendarDays();
        return 	<div id="YBCalendarWeek">
                    { calendarDays }
                </div>;
    }
});

module.exports.generaYBCalendarWeek = function(objAtts, objFuncs)
{
    var firstWeekDay = objAtts.week == 0 ? 0 : objAtts.week * 7;

    return  <YBCalendarWeek
                name = { objAtts.name }
                key = { objAtts.name }
                YB = { objAtts.YB }
                LAYOUT = { objAtts.LAYOUT }
                items = { objAtts.items }
                aplic = { objAtts.aplic }
                prefix = { objAtts.prefix }
                urlStatic = { objAtts.staticurl }
                firstDay = { objAtts.firstDay }
                totalDays = { objAtts.totalDays }
                month = { objAtts.month }
                year = { objAtts.year }
                firstWeekDay = { firstWeekDay }/>;
};
