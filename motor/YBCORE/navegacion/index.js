//Fichero principal para realizar extensiones
module.exports = {
    "URLResolver": require("./URLResolver.js"),
    "ReactLoader": require("./ReactLoader.js").default,
    "helpers": require("./helpers.js"),
};
