import re

from YBLEGACY.FLSqlQuery import FLSqlQuery
from YBLEGACY.FLManager import FLManager
from YBLEGACY.constantes import *
from YBUTILS.viewREST import factorias
from YBUTILS.viewREST import YBLayout
from YBUTILS.viewREST import clasesBase


def generaQueryTable(query, name, templateSchema, mimodel=None, initFilter=None):
    q, count = _setQuery(query, templateSchema, initFilter)
    campos = _dameCamposQuery(q)
    SCHEMA = _dameSchemaTable(campos, name, mimodel)
    IDENT = _dameIdent(templateSchema, count, query)
    DATA = _dameDataQuery(q, campos, name, mimodel)
    # Si en el futuro queremos tabla por query por defecto
    # LAYOUT = _dameLayoutTable(campos, name)
    LAYOUT = {}
    OTROS = {}
    # print(initFilter)
    if initFilter and "otros" in initFilter:
        OTROS = initFilter["otros"]
    if initFilter and "filter" in initFilter:
        for f in initFilter["filter"]:
            IDENT["FILTER"][f] = initFilter["filter"][f]
    # Porque funciona?,  mantener el filtro tras lanzar una accion
    IDENT["FILTER"].update(templateSchema["query"])
    return LAYOUT, IDENT, DATA, SCHEMA, {"verbose_name": "", "type": "query"}, OTROS


def _setQuery(query, querystring, initFilter):
    limit = ""
    whereParam, orderbyParam = clasesBase.dameParamsFiltros(querystring["query"])
    if("p_l" in querystring["query"]):
        limit += " LIMIT " + str(querystring["query"]["p_l"])
    if("p_o" in querystring["query"]):
        limit += " OFFSET " + str(querystring["query"]["p_o"])

    if not limit:
        limit = " LIMIT 50 "

    if "database" not in query:
        query["database"] = None

    countQuery = FLSqlQuery(cx=query["database"])
    countQuery.setTablesList(query["tablesList"])
    if "selectcount" not in query:
        query["selectcount"] = u"COUNT(*) as count"
    countQuery.setSelect(query["selectcount"])
    countQuery.setFrom(query["from"])
    where = query["where"] if "where" in query else ""
    if whereParam:
        where += " AND (" + whereParam + ")"
    if initFilter and "where" in initFilter:
        where += initFilter["where"]

    countQuery.setWhere(where)
    if not countQuery.exec_():
        print("fallo count")

    if countQuery.next():
        count = countQuery.value("count")
    else:
        count = 0

    q = FLSqlQuery(cx=query["database"])
    q.setTablesList(query["tablesList"])
    q.setSelect(query["select"])
    q.setFrom(query["from"])

    groupby = " GROUP BY " + query["groupby"] if "groupby" in query else ""
    # Extrae where y order de request.query_params
    orderby = orderbyParam if orderbyParam else query["orderby"] if "orderby" in query else ""
    orderby = " ORDER BY " + orderby if orderby else ""
    where = query["where"] if "where" in query else ""
    if whereParam:
        where += " AND (" + whereParam + ")"
    if initFilter and "where" in initFilter:
        where += initFilter["where"]

    q.setWhere(where + groupby + orderby + limit)

    if not q.exec_():
        print("no se pudo ejecutar consulta querytable")
        return False
    return q, count


def _dameDataQuery(q, campos, name, mimodel):
    try:
        calculateFields = mimodel.getForeignFields(None, name)
        if not isinstance(calculateFields, (list, tuple)):
            calculateFields = []
    except Exception as e:
        print("Error inesperado en campos calculados de querytable", e)
        calculateFields = []
        pass

    DATA = []
    while q.next():
        fila = {}
        i = 0
        for c in campos:
            if i == 0:
                fila["pk"] = q.value(c)
                i = i + 1
            aField = c.lower().split(" as ")
            if len(aField) == 2:
                fila[aField[1]] = q.value(c)
            else:
                fila[c] = q.value(c)
        for field in calculateFields:
            fun = getattr(mimodel, field["func"])
            fila[field["verbose_name"]] = fun(fila)
        DATA.append(fila)
    return DATA


def _dameCamposQuery(q):
    campos = []
    # TODO usar regex para sacar solo los campos e ignorar comas dentro de ()?
    # for x in q.select().split(","):
    for x in re.split(",(?![^(]*\))", q.select()):
        if x.lower().startswith("distinct("):
            x = x[9:-1]
        campos.append(x.strip())
    return campos


def _dameSchemaTable(campos, name, mimodel):
    SCHEMA = {}
    mng = FLManager()
    for x in campos:
        aField = x.lower().split(" as ")
        # TODO poder usar as en los campos de la query Revisar
        # if len(aField) == 2 and len(aField[0].split(".")) <= 1:
        if len(aField) == 2:
            table = aField[0]
            field = aField[1]
            SCHEMA[field] = {}
            SCHEMA[field]["verbose_name"] = field
            SCHEMA[field]["help_text"] = ""
            SCHEMA[field]["locked"] = False
            SCHEMA[field]["field"] = False
            SCHEMA[field]["required"] = False
            SCHEMA[field]["tipo"] = 3
        else:
            dField = x.split(".")
            table = dField[0]
            field = dField[1]
            if len(aField[0].split(".")) > 1:
                asField = aField[0].split(".")
                table = asField[0]
                field = asField[1]
                if len(aField) == 2:
                    x = asField[1]

            try:
                tMtd = mng.metadata(table)
                fMtd = tMtd.field(field)
                SCHEMA[x] = {}
                SCHEMA[x]["verbose_name"] = fMtd.alias()
                SCHEMA[x]["help_text"] = ""
                SCHEMA[x]["locked"] = False
                SCHEMA[x]["field"] = False
                SCHEMA[x]["required"] = False
                SCHEMA[x]["tipo"] = fMtd.type()
                try:
                    if "optionslist" in fMtd._miField._legacy_mtd:
                        SCHEMA[x]["optionslist"] = fMtd._miField._legacy_mtd["optionslist"]
                        SCHEMA[x]["tipo"] = 5
                except Exception:
                    pass
                if fMtd._miField.rel:
                    SCHEMA[x]["rel"] = fMtd._miField.related_model._meta.db_table
                    # Buscamos desc y verbose_name de desc si existe el modelo
                    try:
                        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(fMtd._miField.related_model._meta.db_table, None)
                        descFunc = serializador.Meta.model.getDesc
                        if descFunc:
                            desc = descFunc()
                        if not desc or desc is None:
                            desc = serializador.Meta.model._meta.pk.name
                            # print(" - Default desc to " + serializador.Meta.model._meta.verbose_name + ": " + desc)
                        SCHEMA[x]["desc"] = desc
                        SCHEMA[x]["verbose_name"] = YBLayout.getYBschema(serializador)[0][desc]["verbose_name"]
                    except Exception as e:
                        # print(e)
                        pass
                    SCHEMA[x]["to_field"] = fMtd._miField.to_fields[0]
                # SCHEMA[x]["rel"] = None
                # SCHEMA[x]["to_field"] = None
            except Exception as e:
                print(e)
                print("No está registrada la tabla", table, ". Se añadirá un schema por defecto.")

                SCHEMA[x] = {}
                SCHEMA[x]["verbose_name"] = x
                SCHEMA[x]["help_text"] = ""
                SCHEMA[x]["locked"] = False
                SCHEMA[x]["field"] = False
                SCHEMA[x]["required"] = False
                SCHEMA[x]["tipo"] = 3
    # SCHEMA por defecto para campos calculados
    try:
        calculateFields = mimodel.getForeignFields(None, name)
        if not isinstance(calculateFields, (list, tuple)):
            calculateFields = []
    except Exception as e:
        print("Error inesperado en campos calculados de querytable", e)
        calculateFields = []
        pass
    for field in calculateFields:
        SCHEMA[field["verbose_name"]] = {}
        SCHEMA[field["verbose_name"]]["verbose_name"] = field["verbose_name"]
        SCHEMA[field["verbose_name"]]["help_text"] = ""
        SCHEMA[field["verbose_name"]]["locked"] = False
        SCHEMA[field["verbose_name"]]["field"] = False
        SCHEMA[field["verbose_name"]]["required"] = False
        SCHEMA[field["verbose_name"]]["tipo"] = 3
    return SCHEMA


def _dameLayoutTable(campos, name):
    LAYOUT = {}
    columns = _dameColumnsLayoutTable(campos)
    LAYOUT["queryGrid_" + name] = {}
    LAYOUT["queryGrid_" + name] = {
        "componente": "YBGrid",
        "label": "tablaQuery",
        "prefix": name,
        "filter": "buscador",
        "columns": columns,
        "rowclick": ""
    }

    return LAYOUT


def _dameColumnsLayoutTable(campos):
    columns = []
    for c in campos:
        columns.append({"tipo": "field", "key": c})
    return columns


def _dameIdent(schema, count, query):
    IDENT = {"FILTER": {}, "MAINFILTER": {}, "ORDER": None, "APLIC": None, "PAG": {"NO": None, "PO": None, "COUNT": count}}
    if "orderby" in query:
        IDENT["FILTER"] = _dameOrderIdent(query["orderby"])
        IDENT["MAINFILTER"] = _dameOrderIdent(query["orderby"])
    if "p_l" in schema["query"]:
        IDENT["FILTER"]["p_l"] = schema["query"]["p_l"]
        IDENT["MAINFILTER"]["p_l"] = schema["query"]["p_l"]
        no = int(schema["query"]["p_l"])
        po = None
        if "p_o" in schema["query"]:
            no = int(schema["query"]["p_l"]) + int(schema["query"]["p_o"])
            po = int(schema["query"]["p_o"]) - int(schema["query"]["p_l"])
            if po > 0:
                IDENT["PAG"]["PO"] = po
        IDENT["PAG"]["NO"] = no
    else:
        IDENT["FILTER"]["p_l"] = 100
        IDENT["MAINFILTER"]["p_l"] = 100
    # IDENT["MAINFILTER"] = IDENT["FILTER"]
    return IDENT


# def _getFieldType(typecode):
#     if typecode == 1043:
#         return 3
#     elif typecode == 701:
#         return 19
#     else:
#         return 3

def _dameOrderIdent(orderby):
    fields = orderby.split(",")
    ORDER = {}
    count = 1
    for f in fields:
        aux = f.strip()
        if aux.find("DESC") > 0:
            aux = "-" + aux[:-4].strip()
        elif aux.find("ASC") > 0:
            aux = aux[:-3].strip()
        ORDER["o_" + str(count)] = aux
        count += 1
    return ORDER
