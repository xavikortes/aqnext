import json

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from rest_framework import viewsets, exceptions
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from YBWEB.ctxJSON import templateCTX
from YBUTILS import mylogging as log
from YBUTILS import queryTable
from YBUTILS import jsonTable
from YBUTILS import gesDoc
from YBUTILS.viewREST import factorias
from YBUTILS.viewREST import filtersPagination
from YBUTILS.viewREST import YBLayout
from YBUTILS.viewREST import cacheController
from YBUTILS.viewREST import helpers
from YBUTILS.viewREST import fileAttachment
from YBUTILS.viewREST import serializers
from YBUTILS.viewREST import accessControl
from YBUTILS.viewREST import wsController
from YBUTILS.viewREST.accion import generaCursor


class YBRModelViewSetBase(viewsets.ModelViewSet, log.logMixin):
    """
        Clase Base que incluye parametrizacion
    """

    pagination_class = filtersPagination.YBPagination
    filter_backends = (filtersPagination.YBFilterBackend,)

    def __init__(self, *args, **kwargs):
        super(YBRModelViewSetBase, self).__init__(*args, **kwargs)
    # --------------------------METODOS A SOBREESCRIBIR-----------------------
    _aplicacion = ""
    _prefix = ""  # Sera el modelName por defecto tambien
    _model = None
    # ------------------------------------------------------------------------

    # --------------------------METODOS FUNCIONALIDADES ADICIONALES-----------

    # METODOS PARA OBTENER APLICACION Y modelo/prefijo
    def getModelName(self):
        return self._prefix

    def getAplic(self):
        return self._aplicacion

    def YBgetModel(self):
        return self._model

    # Sobreescribe metodos para obtener queryset y clase serializadora
    def get_queryset(self):
        # Tratemiento de propiedades de busqueda del queryset
        return self.YBgetModel().objects.all()

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        metaKwargs = kwargs.pop("YB_meta_kwargs", {})
        # I
        serializer_class = self.get_serializer_class(meta_kwargs=metaKwargs)
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self, meta_kwargs=None):
        return factorias.FactoriaSerializadoresBase.getRepositorioWithMeta(modelName=self._prefix, model=self._model, meta=meta_kwargs)

    # Metodos directos para obten
    def getdataInd(self):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return serializer.data

    def getdataMult(self, bPagination=False):
        queryset = self.filter_queryset(self.get_queryset())
        if bPagination:
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return serializer.data


class YBRESTModelViewSet(YBRModelViewSetBase):
    """Incluye funcionalidad REST basica
        update, add...
    """

    def __init__(self, *args, **kwargs):
        super(YBRESTModelViewSet, self).__init__(*args, **kwargs)

    @classmethod
    def cargaListaQuery(self, prefix, request):
        params = None
        if not request.query_params:
            params = request.data
        else:
            params = request.query_params
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, None)
        mimodel = serializador.Meta.model
        # try:
        #     template = "master"
        #     calculateFields = mimodel.getForeignFields(None, template)
        #     for field in calculateFields:
        #         serializador._declared_fields.update({field["verbose_name"]: serializers.serializers.ReadOnlyField(label=field["verbose_name"], source=field["func"])})

        # except Exception as e:
        # except Exception:
        #     # print("Error inesperado en campos calculados de caraglistaquery", e)
        #     pass
        if "qr_pk" not in params or not params["qr_pk"]:
            queryTableFunc = getattr(mimodel, "queryGrid_" + params["qr_t"])
        else:
            queryTableFunc = getattr(mimodel.objects.all().get(pk=params["qr_pk"]), "queryGrid_" + params["qr_t"])

        # where, orderby = queryTable.dameParamsFiltros(request.query_params)
        try:
            query = queryTableFunc(None)
        except Exception:
            query = queryTableFunc()

        if query:
            qparams = {}
            qparams["query"] = params
            data = {}
            resp = {}
            LAYOUT, data["IDENT"], data["DATA"], data["SCHEMA"], data["META"], a = queryTable.generaQueryTable(query, params["qr_t"], qparams, mimodel)
            resp["PAG"] = data["IDENT"]["PAG"]
            resp["data"] = data["DATA"]
            return Response(resp)

    @helpers.decoradores.checkAuthentication
    def list(self, request, *args, **kwargs):
        params = request.query_params
        if not request.query_params:
            params = request.data
        # Si es una queryTable invocamos a su funcion
        if "qr_t" in params:
            return self.cargaListaQuery(self._prefix, request)

        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        # Permite establecer los campos mediante fs
        listaCampos = list(filtersPagination._filterDict(request.query_params, "fs_").keys())
        vYB_meta_kwargs = {}
        if len(listaCampos) > 0:
            vYB_meta_kwargs = {"fields": tuple(listaCampos)}
        if page is not None:
            serializer = self.get_serializer(page, many=True, YB_meta_kwargs=vYB_meta_kwargs)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True, YB_meta_kwargs=vYB_meta_kwargs)
        return Response(serializer.data)

    @helpers.decoradores.checkAuthentication
    def quicklist(self, request, *args, **kwargs):
        """
            Permite acceder a pk - descripcion para obtener listas rapidas
        """
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, YB_meta_kwargs={"fields": ("pk", "desc")})
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True, YB_meta_kwargs={"fields": ("pk", "desc")})
        return Response(serializer.data)

    @helpers.decoradores.checkAuthentication
    def detailedlist(self, request, *args, **kwargs):
        """
            Serializa con un nivel superior en el arbol de relaciones
        """
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, YB_meta_kwargs={"depth": 1})
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True, YB_meta_kwargs={"depth": 1})
        return Response(serializer.data)

    @helpers.decoradores.checkAuthentication
    def accionlist(self, request, tipo, *args, **kwargs):
        # TODO->PEndiente control por seguridad
        acciones = factorias.FactoriaAccion.getAcciones(self.getModelName(), tipo)
        return Response(acciones.keys())

    @helpers.decoradores.checkAuthentication
    def ejecutaraccionFileInd(self, request, accion=None, pk=None, *args, **kwargs):
        # print("ejecutando accion file", pk, accion, request.data, request.FILES)
        accion = accion.lower()
        miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "I").get(accion, None)
        if miaccion:
            miaccionobj = miaccion(pk=pk)
            data = {}
            data["oParam"] = request.data
            data["oParam"]["FILES"] = request.FILES
            resul = miaccionobj.ejecutarExterna(data, request)
            if "autoUpload" in request.data and request.data["autoUpload"] == "false":
                return Response(resul)

            if request.FILES:
                gesDoc.fileUpload(self._prefix, pk, request.FILES)
            return Response(resul)
        else:
            log.error("Accion individual no registrada %s", accion)
            print(_("Accion no existente %s") % accion)
            raise exceptions.APIException(_("Accion no existente %s") % accion)

    @helpers.decoradores.checkAuthentication
    def ejecutaraccionInd(self, request, accion=None, pk=None, *args, **kwargs):
        # Obtener accion
        accion = accion.lower()
        miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "I").get(accion, None)
        if pk == "NF":
            miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "O").get(accion, None)
            pk = None
        if miaccion:
            # Control de acciones BULK
            if (pk == "_BULK") or ("a_BULK" in request.query_params):
                # Cambiamos serializador de entrada por este
                bconDatosIN = True
                # opcion NORESUL
                bRESUL = not ("a_NORESUL" in request.query_params)
                milista = request.data
                resul2 = list()
                for elem in milista:
                    miaccionobj = miaccion(pk=elem["PK"])
                    if bconDatosIN:
                        oparam = {}
                        oparam["oParam"] = elem["DATAIN"]
                        resul = miaccionobj.ejecutarExterna(oparam, request)
                    else:
                        resul = miaccionobj.ejecutarExterna({}, request)
                    if bRESUL:
                        resul2.append(resul)
                return Response(resul2)
            # ACCIONES NORMALES
            else:
                miaccionobj = miaccion(pk=pk)
                resul = miaccionobj.ejecutarExterna(request.data, request)
                return Response(resul)
        else:
            log.error("Accion individual no registrada %s", accion)
            print(_("Accion no existente %s") % accion)
            raise exceptions.APIException(_("Accion no existente %s") % accion)

    # @csrf_exempt
    def ejecutaraccionCsrAux(self, request, accion=None, *args, **kwargs):
        key = None
        if "HTTP_KEY" in request.META:
            key = request.META["HTTP_KEY"]
        source = None
        if "HTTP_SOURCE" in request.META:
            source = request.META["HTTP_SOURCE"]
        contentType = None
        if "CONTENT_TYPE" in request.META:
            contentType = request.META["CONTENT_TYPE"]
        remoteAddr = None
        if "REMOTE_ADDR" in request.META:
            remoteAddr = request.META["REMOTE_ADDR"]
        try:
            postParam = {}
            if contentType and contentType == "application/xml":
                postParam["POST"] = {"xml": request.body.decode("utf-8")}
            else:
                postParam["POST"] = json.loads(request.body.decode("utf-8"))
        except Exception as e:
            print(e)
            postParam = filtersPagination._generaPostParam(request._data)
        try:
            miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "csr").get(accion, None)
            postParam["POST"]["key"] = key
            postParam["POST"]["source"] = source
            postParam["POST"]["contentType"] = contentType
            postParam["POST"]["remoteAddr"] = remoteAddr
            if miaccion:
                return Response(miaccion(postParam["POST"]))
            else:
                print("Error inesperado miaccion")
                raise exceptions.APIException(_("Accion no existente %s") % accion)
        except Exception as e:
            print("Error inesperado invocacion", e)
            raise exceptions.APIException(_("Accion no existente %s") % accion)

    # # @csrf_exempt
    # def ejecutaraccionCsrAuxDownFile(self, request, accion=None, *args, **kwargs):
    #     try:
    #         print(factorias.FactoriaAccion.getAcciones(self.getModelName(), "csr"))
    #         miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "csr").get(accion, None)
    #         if miaccion:
    #             return miaccion("2")
    #         else:
    #             print("Error inesperado miaccion")
    #             raise exceptions.APIException(_("Accion no existente %s") % accion)
    #     except Exception as e:
    #         print("Error inesperado invocacion", e)
    #         raise exceptions.APIException(_("Accion no existente %s") % accion)

    # @csrf_exempt
    def ejecutaraccionCsr(self, request, accion=None, pk=None, *args, **kwargs):
        try:
            postParam = {}
            postParam["POST"] = json.loads(request.body.decode("utf-8"))
        except Exception:
            postParam = filtersPagination._generaPostParam(request._data)
        # Obtener accion
        accion = accion.lower()
        miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "I").get(accion, None)
        if miaccion:
            miaccionobj = miaccion(pk=pk)
            resul = miaccionobj.ejecutarExterna(request.data, request, postParam)
            return Response(resul["resul"])
        else:
            log.error("Accion individual no registrada %s", accion)
            print(_("Accion no existente %s") % accion)
            raise exceptions.APIException(_("Accion no existente %s") % accion)

    def ejecutaraccionList(self, request, accion=None, *args, **kwargs):
        accion = accion.lower()
        miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "Q").get(accion, None)
        if miaccion:
            query = dict()
            query["ORDER"] = filtersPagination._getOrder(request.query_params, "o_")
            query["FILTER"] = filtersPagination._getsearchParam(request.query_params, self.get_serializer(partial=True), None)
            miaccionobj = miaccion(query=query)
            resul = miaccionobj.ejecutarExterna(request.data, request)
            return Response(resul)
        else:
            # TODO: Tratamiento de acciones individuales como multiples
            miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "I").get(accion, None)
            bRESUL = not ("a_NORESUL" in request.query_params)
            if miaccion:
                queryset = self.filter_queryset(self.get_queryset())
                resul2 = list()
                for obj in queryset:
                    miaccionobj = miaccion(pk=obj.pk)
                    resul = miaccionobj.ejecutarExterna(request.data, request)
                    if bRESUL:
                        resul2.append(resul)
                return Response(resul2)
            else:
                log.error("Accion Query no registrada %s", accion)
                raise exceptions.APIException(_("Accion no existente %s") % accion)

    @helpers.decoradores.checkAuthentication
    def generaCSVAux(self, request, accion=None, *args, **kwargs):
        return None

    @helpers.decoradores.checkAuthentication
    def generaCSVInd(self, request, accion=None, pk=None, *args, **kwargs):
        params = request.query_params or None
        response = fileAttachment.generaCSV(self._prefix, pk, accion, params)
        return response

    @helpers.decoradores.checkAuthentication
    def generaJReportInd(self, request, accion=None, pk=None, *args, **kwargs):
        response = fileAttachment.generaJReport(self._prefix, pk, accion)
        return response

    @helpers.decoradores.checkAuthentication
    def generaJReport(self, request, report=None, pk=None, *args, **kwargs):
        response = fileAttachment.getJReport(pk, report)
        return response

    @helpers.decoradores.checkAuthentication
    def generaAttachment(self, request, pk=None, *args, **kwargs):
        response = gesDoc.generaFiles(self._prefix, pk)
        return response

    @helpers.decoradores.checkAuthentication
    def ejecutaraccionAux(self, request, accion=None, *args, **kwargs):
        accion = accion.lower()
        miaccion = factorias.FactoriaAccion.getAcciones(self.getModelName(), "O").get(accion, None)
        if miaccion:
            if "a_BULK" in request.query_params:
                bRESUL = not ("a_NORESUL" in request.query_params)
                serializador = miaccion.serializerClassIN(many=True, data=request.data)
                serializador.is_valid(raise_exception=True)
                milista = serializador.validated_data
                resul2 = list()
                for elem in milista:
                    miaccionobj = miaccion()
                    resul = miaccionobj.ejecutarExterna(elem, request)
                    if bRESUL:
                        resul2.append(resul)
                return Response(resul2)
            else:
                miaccionobj = miaccion()
                resul = miaccionobj.ejecutarExterna(request.data, request)
                return Response(resul)
        else:
            log.error("Accion Otra no registrada %s", accion)
            print(_("Accion no existente %s") % accion)
            raise exceptions.APIException(_("Accion no existente %s") % accion)
        pass


class YBMIXINCTXtemplate(object):
    """
        Permite incluir funcionalidad defecto sin depender de otros temas
    """

    @classmethod
    def getDefaultLayOUT(cls, schema):
        return YBLayout.getDefaultLayOUT(schema)

    @classmethod
    def incluyeContextTipo(cls, ctxrequest, prefix, tipo, modif, template, param):
        serializador = None
        dict = {
            "YB": {
                "IDENT": {
                    "PREFIX": prefix,
                    "MODIF": modif,
                    "TIPO": tipo,
                    "NOMBRE": template
                },
                "PARAM": param,
                "OPTS": {}
            }
        }
        if tipo == "accion":
                if modif:
                    serializador = factorias.FactoriaAccion.getAccionNom(prefix, modif).serializerClassIN
                    dict["YB"]["SCHEMA"], dict["YB"]["META"] = YBLayout.getYBschema(serializador)
                    dict["YB"]["LAYOUT"] = cls.getDefaultLayOUT(dict["YB"]["SCHEMA"])
                else:
                    pass
        else:
            # Obtenemos serializador por defecto del modelo
            try:
                serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix)
                dict["YB"]["SCHEMA"], dict["YB"]["META"] = YBLayout.getYBschema(serializador)
                dict["YB"]["LAYOUT"] = cls.getDefaultLayOUT(dict["YB"]["SCHEMA"])
            except Exception:
                pass
        ctxrequest.update(dict)
        return serializador

    @classmethod
    def cargaDatosPK(self, prefix, pk, user, template=None):
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, prefix, None, template)
        mimodel = serializador.Meta.model
        try:
            if not template:
                template = "master"
            calculateFields = mimodel.getForeignFields(None, template)
            for field in calculateFields:
                serializador._declared_fields.update({field["verbose_name"]: serializers.serializers.ReadOnlyField(label=field["verbose_name"], source=field["func"])})

        except Exception:
            # print("Error inesperado en campos calculados de cargadatospk", e)
            pass
        # Para simplificar cogemos model del meta
        obj = serializador.Meta.model.objects.all().get(pk=pk)
        schema, meta = YBLayout.getYBschema(serializador)
        miser = serializador(instance=obj)
        return {"DATA": miser.data, "SCHEMA": schema, "META": meta}

    @classmethod
    def cargaDatosCustom(self, prefix, name, pk, schema, method, template):
        print(prefix, name, pk, schema, method, template)
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, None)
        mimodel = serializador.Meta.model
        funcName = schema["custom"]
        try:
            if not pk:
                queryTableFunc = getattr(mimodel, funcName)
            else:
                queryTableFunc = getattr(mimodel.objects.all().get(pk=pk), funcName)
            data = queryTableFunc(template)

            return {}, data, {}, {}, {}
        except Exception as e:
            print(e)
            return {}, {}, {}, {}, {}

    @classmethod
    def cargaDatosqueryTable(self, prefix, name, pk, schema, method, template):
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, None)
        mimodel = serializador.Meta.model
        initFilter = None
        if method == "GET":
            try:
                if not pk:
                    initFilterFunc = getattr(mimodel, "queryGrid_" + name + "_initFilter")
                else:
                    initFilterFunc = getattr(mimodel.objects.all().get(pk=pk), "queryGrid_" + name + "_initFilter")
                if initFilterFunc:
                    try:
                        initFilter = initFilterFunc(template)
                    except Exception:
                        initFilter = initFilterFunc()
            except Exception as e:
                print("exception init filter", e)
                pass
        try:
            if not pk:
                queryTableFunc = getattr(mimodel, "queryGrid_" + name)
            else:
                queryTableFunc = getattr(mimodel.objects.all().get(pk=pk), "queryGrid_" + name)
            try:
                data = queryTableFunc(template)
            except Exception:
                data = queryTableFunc()

            if data:
                return queryTable.generaQueryTable(data, name, schema, mimodel, initFilter)

            return {}, {}, {}, {}, {}, {}
        except Exception as e:
            print(e)
            return {}, {}, {}, {}, {}, {}

    @classmethod
    def cargaDatosJSONTable(self, data, name, schema):
        return jsonTable.generaJsonTable(data, name, schema)

    @classmethod
    def cargaDatosCustomfilter(self, table, schema, usuario):
        try:
            filters = filtersPagination._getGridFilters(table, usuario)
            return filters
        except Exception as e:
            print("Customfilter____", e)
            return {}

    @classmethod
    def cargaDatosSchema(self, prefix, schema, pk, template):
        # Si esta relacionado caragamos apartir de la pk de el padre
        YB = {}

        if "rel" in schema:
            queryDict = schema["querystring"]
            rel = "s_" + schema["rel"] + "__exact"
            queryDict[rel] = pk
            YB["IDENT"], YB["DATA"], YB["SCHEMA"], YB["META"] = self.cargaDatosQuery(prefix, queryDict, template)
        else:
            # Si no cargamos datos de query
            queryDict = schema["querystring"]
            YB["IDENT"], YB["DATA"], YB["SCHEMA"], YB["META"] = self.cargaDatosQuery(prefix, queryDict, template)

        return YB

    @classmethod
    def cargaDatosReport(self, name, schema, pk, template):
        try:
            prefix = schema["reportTable"]
            serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, None)
            mimodel = serializador.Meta.model
            if not pk:
                reportFunc = getattr(mimodel, "report_" + name)
            else:
                cursor = generaCursor.getCursor(mimodel, pk)
                reportFunc = getattr(mimodel.objects.all().get(pk=pk), "report_" + name)
            data = reportFunc(cursor)
            return data
        except Exception as e:
            # Si no existe funcion report devuelve objeto vacio y avisa
            print("No se encuentra report_", name, " tabla: ", schema["reportTable"], " ", e)
            return {}

    @classmethod
    def cargaDatosPrefixSchema(self, prefix, schema, pk, template):
        # Si esta relacionado caragamos apartir de la pk de el padre
        YB = {}

        # if "rel" in schema:
        #     queryDict = schema["querystring"]
        #     rel = "s_" + schema["rel"] + "__exact"
        #     queryDict[rel] = pk
        #     YB["IDENT"], YB["DATA"], YB["SCHEMA"], YB["META"] = self.cargaDatosQuery(prefix, queryDict, template)
        # else:
        #     # Si no cargamos datos de query
        alias = prefix
        queryDict = schema["querystring"]
        if "prefix" in schema:
            prefix = schema["prefix"]
        YB["IDENT"], YB["DATA"], YB["SCHEMA"], YB["META"] = self.cargaDatosQuery(prefix, queryDict, alias)
        if len(YB["DATA"]) == 1 and "array" not in schema:
            YB["DATA"] = YB["DATA"][0]
        return YB

    @classmethod
    def cargaDatosQuery(self, prefix, query, template=None):
        # TODO ver si tiene permiso para ver
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, template=template)
        mimodel = serializador.Meta.model
        try:
            if not template:
                template = "master"
            calculateFields = mimodel.getForeignFields(None, template)
            for field in calculateFields:
                serializador._declared_fields.update({field["verbose_name"]: serializers.serializers.ReadOnlyField(label=field["verbose_name"], source=field["func"])})

        # except Exception as e:
        except Exception:
            # print("Error inesperado en campos calculados de cargadatosquery", e)
            pass
        try:
            # IDENT
            (filtros, miorder, obj, queryset) = filtersPagination._filter_queryset2(query, mimodel.objects.all(), serializador, template=template)
            filtros = queryset
            mipag = filtersPagination.YBPagination()
            # Añadido bCount para la paginacion)
            bCount2 = False or ("p_c" in query)
            obj = mipag.paginate_queryset2(obj, query, bCount2)

            # SCHEMA, META
            schema, meta = YBLayout.getYBschema(serializador)
            miser = serializador(instance=obj, many=True)
            # GetParam especiales ya no se usa, pero podria limitar el numero de campos que enviamos con data y schema
            SCHEMA_UPD, DATA_UPD = filtersPagination._getParamEspeciales(queryset, serializador())
            # DATA
            if DATA_UPD != {}:
                miser.data.update(DATA_UPD)

            # este actualiza propiedades
            for k, v in SCHEMA_UPD.items():
                if "related" in v:
                    schema[k] = v
                try:
                    schema[k].update(v)
                except Exception:
                    pass

            # METADATA
            # TODO de donde sacamos los metadatos?
            # metadata = YBLayout.getYBMetadata(serializador, miser.data)
            return {"MAINFILTER": query, "FILTER": filtros, "ORDER": miorder, "PAG": {"NO": mipag.get_next_offset(), "PO": mipag.get_previous_offset(), "COUNT": mipag.count}}, miser.data, schema, meta
        except Exception as e:
            log.error("Error cargaDatosQuery: \n%s", e)
            return {"MAINFILTER": "", "FILTER": "", "ORDER": "", "PAG": {}}, [], {}, {}

    @classmethod
    def cargaDatosFormNewRecord(self, prefix, request):
        data = {}

        accion = "iniciaValores"
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, "newRecord")

        data["SCHEMA"], data["META"] = YBLayout.getYBschema(serializador)
        try:
            miaccion = factorias.FactoriaAccion.getAcciones(prefix, "O").get(accion, None)
            miaccionconobj = miaccion(request)
            d = miaccionconobj.ejecutarExterna(request)
            data["DATA"] = d["data"]
        except Exception as e:
            print(e)
            data = {}

        return data

    @classmethod
    def cargaDatosLabels(self, prefix, pk, request, template=None, data=None):
        try:
            serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, None)
            mimodel = serializador.Meta.model
            if not pk:
                # TODO en vez de cursor enviar el filtro del maestro??
                labels = mimodel.iniciaValoresLabel(None, template, None, data)
            else:
                cursor = generaCursor.getCursor(mimodel, pk)
                labels = mimodel.objects.all().get(pk=pk).iniciaValoresLabel(template, cursor, data)

            if not labels:
                return {}
            else:
                return labels
        except Exception as e:
            # Si no existe funcion iniciavaloreslabel devuelve objeto vacio y avisa
            print(e)
            return {}

    @classmethod
    def cargaDatosChat(self, user):
        chatObj = wsController.chatController.getChat(user)
        return chatObj

    @classmethod
    def dameWS(self, schema):
        # wsObj = wsController.wsController.getWS()
        if "ws" in schema:
            return schema["ws"]
        return False

    @classmethod
    def initValidation(self, aplic, prefix, schema, template, request, data=None):
        # sessionUser = request.user.username
        params = filtersPagination._generaParamFromRequest(request.query_params)
        if not data:
            data = {}
            data["params"] = params
        else:
            data["params"] = params
        serializador = factorias.FactoriaSerializadoresBase.getRepositorio(prefix, template)
        mimodel = serializador.Meta.model
        # try:
        #     if not template:
        #         template = "master"
        #     calculateFields = mimodel.getForeignFields(None, template)
        #     for field in calculateFields:
        #         serializador._declared_fields.update({field["verbose_name"]: serializers.serializers.ReadOnlyField(label=field["verbose_name"], source=field["func"])})

        # except Exception as e:
        #     # print("Error inesperado en campos calculados de initvalidation ", e)
        #     pass
        try:
            if not mimodel.initValidation(template, data):
                # TODO añadir root url
                url = "/" + schema["error"]["aplic"] + "/" + schema["error"]["prefix"] + "/custom/" + schema["error"]["template"]
                if schema["error"]["template"] == "master":
                    url = "/" + schema["error"]["aplic"] + "/" + schema["error"]["prefix"] + "/" + schema["error"]["template"]
                msg = ""
                if "msg" in schema["error"]:
                    msg = schema["error"]["msg"]
                    url += "?m_error=" + msg
                return url
        except Exception as e:
            print(e)
            print("No esta declarada la funcion initValidation para ", self._prefix)
            pass

        return False


class YBLayOutModelViewSet(viewsets.ViewSet, log.logMixin, YBMIXINCTXtemplate, APIView):
    permission_classes = (IsAuthenticated,)
    appLabels = None
    """
        Incluye funcionalidad Visualizacion basica
    """
    # A IMPLEMENTAR AL DEFINIR CLASE
    _aplicacion = ""
    _prefix = ""

    # INICIALIZACION
    def __init__(self, *args, **kwargs):
        super(YBLayOutModelViewSet, self).__init__(*args, **kwargs)

    def dameAppLabel(self, aplic):
        if self.appLabels is None:
            self.appLabels = {}
            portalMenu = templateCTX.cargaMenuJSON("portal/menu_portal.json")
            for app in portalMenu["items"]:
                self.appLabels[app["NAME"]] = app["TEXT"]

        if aplic in self.appLabels:
            return self.appLabels[aplic]

        return ""

    def dameAuxData(self, request, pk, template, schema):
        data = {}
        data["otros"] = {}
        data["persistente"] = {}
        data["default"] = {}
        data["labels"] = {}
        filtros = {}
        postOtros = {}
        postLabels = {}
        postDrawif = {}
        querystring = {}
        refresh = None

        if request._method == "PUT":
            postParam = {}
            try:
                postParam["POST"] = json.loads(request.body.decode("utf-8"))
            except Exception:
                if request._data:
                    postParam = filtersPagination._generaPostParam(request._data)
            if "FILTER" in postParam["POST"]:
                filtros = postParam["POST"]["FILTER"]
            if "querystring" in postParam["POST"]:
                querystring = postParam["POST"]["querystring"]
            if "otros" in postParam["POST"]:
                postOtros = postParam["POST"]["otros"] or {}
            if "labels" in postParam["POST"]:
                postLabels = postParam["POST"]["labels"] or {}
            if "drawIf" in postParam["POST"]:
                postDrawif = postParam["POST"]["drawIf"] or {}
            if "refresh" in postParam["POST"]:
                refresh = postParam["POST"]["refresh"]

        try:
            accion = "init"
            drawIf = schema["drawIf"] if "drawIf" in schema else None
            miaccion = factorias.FactoriaAccion.getAcciones(self._prefix, "O").get(accion, None)
            miaccionconobj = miaccion(diI=drawIf, pk=pk)
            dIf = miaccionconobj.ejecutarExterna(diI=drawIf, pk=pk)
        except Exception:
            dIf = {}
            dIf["drawif"] = {}
            dIf["resul"] = False
        data["otros"]["pk"] = pk
        data["persistente"]["pk"] = pk
        data["drawIf"] = postDrawif
        data["otros"].update(postOtros)
        # Quien tiene prioridad en valor de label? cliente o servidor? hay que añadir algo intermedio?
        data["labels"].update(postLabels)
        data["labels"].update(self.cargaDatosLabels(self._prefix, pk, request, template) or {})
        if dIf["drawif"]:
            data["drawIf"].update(dIf["drawif"])
        if "navbar" in schema:
            if not schema["navbar"]:
                data["drawIf"]["navbar"] = False

        ws = self.dameWS(schema)
        chat = self.cargaDatosChat(request.user.username)
        data["chat"] = chat
        if ws:
            data["ws"] = ws

        return data, filtros, querystring, refresh

    def dameDataMaster(self, request, template):
        milist = templateCTX.damechainMasterTemplate(self._aplicacion, self._prefix, template)
        schema = templateCTX.cargaPlantillaJSON(milist)
        YB = {}
        data, filtros, querystring, refresh = self.dameAuxData(request, None, template, schema)

        if "initValidation" in schema:
            # dataPrefix = data[self._prefix] if template != "master" or template != "custom" else None
            url = self.initValidation(self._aplicacion, self._prefix, schema["initValidation"], template, request)
            if url:
                return False, HttpResponseRedirect(url)
        # Mantener filtros al refrescar
        if self._prefix in filtros:
            schema["querystring"] = filtros[self._prefix]
        YB["IDENT"], YB["DATA"], YB["SCHEMA"], YB["META"] = self.cargaDatosQuery(self._prefix, schema["querystring"], template)
        data[self._prefix] = YB

        schema["schema"].update(querystring)
        for table in schema["schema"]:
            save = True
            if refresh:
                if refresh != table:
                    save = False
            if table == self._prefix:
                pass
            elif "custom" in schema["schema"][table] and save:
                data[table] = {}
                data[table]["IDENT"], data[table]["DATA"], data[table]["SCHEMA"], data[table]["META"], data["persistente"] = self.cargaDatosCustom(self._prefix, table, None, schema["schema"][table], request._method, template)
            elif "query" in schema["schema"][table] and save:
                if table in filtros:
                    schema["schema"][table]["query"] = filtros[table]
                data[table] = {}
                LAYOUT = {}
                LAYOUT, data[table]["IDENT"], data[table]["DATA"], data[table]["SCHEMA"], data[table]["META"], data["persistente"] = self.cargaDatosqueryTable(self._prefix, table, None, schema["schema"][table], request._method, template)
            elif "json" in schema["schema"][table]:
                data[table] = {}
                LAYOUT = {}
                LAYOUT, data[table]["IDENT"], data[table]["DATA"], data[table]["SCHEMA"], data[table]["META"] = self.cargaDatosJSONTable(data[self._prefix]["DATA"][0][schema["schema"][table]["json"]], table, schema["schema"][table])
            elif "prefix" in schema["schema"][table] and save:
                if table in filtros:
                    schema["schema"][table]["querystring"] = filtros[table]
                data[table] = self.cargaDatosPrefixSchema(table, schema["schema"][table], None, template)
            elif save:
                if table in filtros:
                    schema["schema"][table]["querystring"] = filtros[table]
                data[table] = self.cargaDatosSchema(table, schema["schema"][table], None, template)
            if "customfilter" in schema["schema"][table]:
                prefix = self._prefix
                if "query" in schema["schema"][table]:
                    prefix = table
                data[prefix]["customfilter"] = self.cargaDatosCustomfilter(table, schema["schema"][table], request.user)
        return data, schema

    def dameDataFormRecord(self, request, pk, template):
        milist = templateCTX.damechainTemplate(self._aplicacion, self._prefix, template)
        schema = templateCTX.cargaPlantillaJSON(milist)
        data, filtros, querystring, refresh = self. dameAuxData(request, pk, template, schema)
        data[self._prefix] = self.cargaDatosPK(self._prefix, pk, request.user, template)
        if "initValidation" in schema:
            dataPrefix = data[self._prefix] if pk != "master" or pk != "custom" else None
            url = self.initValidation(self._aplicacion, self._prefix, schema["initValidation"], template, request, dataPrefix)
            if url:
                return False, HttpResponseRedirect(url)

        # TODO añadir querystring durante una peticion PUT
        schema["schema"].update(querystring)
        for table in schema["schema"]:
            if "customfilter" in schema["schema"][table]:
                data[self._prefix]["customfilter"] = self.cargaDatosCustomfilter(table, schema["schema"][table], request.user)
            if table == self._prefix:
                pass
            if "reportTable" in schema["schema"][table]:
                if "report" not in data:
                    data["report"] = {}
                data["report"][table] = self.cargaDatosReport(table, schema["schema"][table], pk, template)
            elif "query" in schema["schema"][table]:
                if table in filtros:
                    schema["schema"][table]["query"] = filtros[table]
                data[table] = {}
                LAYOUT = {}
                LAYOUT, data[table]["IDENT"], data[table]["DATA"], data[table]["SCHEMA"], data[table]["META"], data["persistente"] = self.cargaDatosqueryTable(self._prefix, table, pk, schema["schema"][table], request._method, template)
                LAYOUT.update(schema["layout"])
                schema["layout"].update(LAYOUT)
            elif "json" in schema["schema"][table]:
                data[table] = {}
                LAYOUT = {}
                LAYOUT, data[table]["IDENT"], data[table]["DATA"], data[table]["SCHEMA"], data[table]["META"] = self.cargaDatosJSONTable(data[self._prefix]["DATA"][schema["schema"][table]["json"]], table, schema["schema"][table])
                LAYOUT.update(schema["layout"])
                schema["layout"].update(LAYOUT)
            elif "fieldRelation" in schema["schema"][table]:
                if table in filtros:
                    schema["schema"][table] = filtros[table]
                fieldRelation = data[self._prefix]["DATA"][schema["schema"][table]["fieldRelation"]]
                data[table] = self.cargaDatosSchema(table, schema["schema"][table], fieldRelation, template)
            else:
                if table in filtros:
                    schema["schema"][table]["querystring"] = filtros[table]
                data[table] = self.cargaDatosSchema(table, schema["schema"][table], pk, template)

        return data, schema

    def dameReturnInvocacion(self, request, pk, data, schema, template):
        # Validaciones inciales
        superuser = request.user.is_superuser
        msg = filtersPagination._generaMsgFromRequest(request.query_params) or None
        # if "initValidation" in schema:
        #     dataPrefix = data[self._prefix] if pk != "master" or pk != "custom" else None
        #     url = self.initValidation(self._aplicacion, self._prefix, schema["initValidation"], template, request, dataPrefix)
        #     if url:
        #         return HttpResponseRedirect(url)

        if "slot" in request.query_params or request._method == "PUT":
            history = cacheController.getHistory(request)
            return Response({"data": data, "history": history})

        usuario = request.user.username
        groups = request.user.groups.all()
        miMenu = {}
        try:
            miMenu = templateCTX.cargaMenuJSON(self._aplicacion + "/menu_" + self._aplicacion + ".json")
        except Exception:
            pass
        history = cacheController.addHistory(request, self._aplicacion, self._prefix, pk, template)
        history = history["list"][history["pos"] - 1] if history["pos"] > 0 else history["list"][history["pos"]]
        appLabel = self.dameAppLabel(self._aplicacion)

        objRender = {"aplic": self._aplicacion, "prefix": self._prefix, "menu": miMenu, "data": data, "layout": schema, "msg": msg, "superuser": superuser, "usuario": usuario, "grupos": groups, "history": history, "aplicLabel": appLabel, "custom": template}
        return render(request, "YBWEB/AQdetail.html", objRender)

    # METODOS DE INVOCACION POR DEFECTO---------------------------------------------------
    @helpers.decoradores.checkAuthentication
    def invocaAQdashboard(self, request):
        usuario = request.user.username
        superuser = request.user.is_superuser

        history = cacheController.addHistory(request, self._aplicacion, "dashboard", None, None)
        history = history["list"][history["pos"] - 1] if history["pos"] > 0 else history["list"][history["pos"]]
        miMenu = templateCTX.cargaMenuJSON(self._aplicacion + "/menu_" + self._aplicacion + ".json")
        appLabel = self.dameAppLabel(self._aplicacion)

        miMenuItems = accessControl.accessControl.dameDashboard(request.user, miMenu["items"])
        objRender = {"aplic": self._aplicacion, "menuJson": miMenuItems, "usuario": usuario, "superuser": superuser, "history": history, "aplicLabel": appLabel}
        if len(miMenuItems) == 1 or ("format" in miMenu and miMenu["format"] == "navBarMenu"):
            url = miMenuItems[0]["URL"]
            return HttpResponseRedirect("/" + url)
        return render(request, "YBWEB/dashboard.html", objRender)

    @helpers.decoradores.checkAuthentication
    def invocaAQNtemplate(self, request, pk, template):
        data, schema = self.dameDataFormRecord(request, pk, template)
        if not data:
            return schema
        return self.dameReturnInvocacion(request, pk, data, schema, template)

    @helpers.decoradores.checkAuthentication
    def invocaAQMtemplate(self, request, template):
        data, schema = self.dameDataMaster(request, template)
        if not data:
            return schema
        return self.dameReturnInvocacion(request, "custom", data, schema, template)

    @helpers.decoradores.checkAuthentication
    def invocaAQN(self, request, pk):
        data, schema = self.dameDataFormRecord(request, pk, "formRecord")
        if not data:
            return schema
        return self.dameReturnInvocacion(request, pk, data, schema, "formRecord")

    @helpers.decoradores.checkAuthentication
    def invocaAQNmaster(self, request):
        data, schema = self.dameDataMaster(request, "master")
        if not data:
            return schema
        return self.dameReturnInvocacion(request, "master", data, schema, None)

    @helpers.decoradores.checkAuthentication
    def formNewRecord(self, request):
        milist = templateCTX.damechainMasterTemplate(self._aplicacion, self._prefix, "newrecord")
        schema = templateCTX.cargaPlantillaJSON(milist)

        data = {}
        data["otros"] = {}
        data["persistente"] = {}
        data["default"] = {}
        data["labels"] = {}
        ws = self.dameWS(schema)
        chat = self.cargaDatosChat(request.user.username)
        data["chat"] = chat
        if ws:
            data["ws"] = ws

        data[self._prefix] = self.cargaDatosFormNewRecord(self._prefix, request)
        for s in schema["schema"]:
            if "create" in schema["schema"][s] and schema["schema"][s]["create"]:
                data[s] = self.cargaDatosFormNewRecord(s, request)
            else:
                data[s] = self.cargaDatosSchema(s, schema["schema"][s], None, "newrecord")

        # Validaciones inciales
        msg = filtersPagination._generaMsgFromRequest(request.query_params) or None
        if "initValidation" in schema:
            name = "newrecord" + self._prefix
            url = self.initValidation(self._aplicacion, self._prefix, schema["initValidation"], name, request, data[self._prefix]["DATA"])
            if url:
                return HttpResponseRedirect(url)

        try:
            accion = "init"
            drawIf = schema["drawIf"] if "drawIf" in schema else None
            miaccion = factorias.FactoriaAccion.getAcciones(self._prefix, "O").get(accion, None)
            miaccionconobj = miaccion(diI=drawIf, data=data[self._prefix]["DATA"])
            dIf = miaccionconobj.ejecutarExterna(diI=drawIf, data=data[self._prefix]["DATA"])
        except Exception:
            dIf = {}
            dIf["drawif"] = {}
            dIf["resul"] = False

        # if params:
        #     for name in params:
        #         data[self._prefix]["DATA"][name] = params[name]
        data["drawIf"] = dIf["drawif"]
        data["labels"].update(self.cargaDatosLabels(self._prefix, None, request, "newrecord") or {})

        history = cacheController.getHistory(request)
        history = history["list"][history["pos"]]
        usuario = request.user.username
        superuser = request.user.is_superuser
        appLabel = self.dameAppLabel(self._aplicacion)

        objRender = {"aplic": self._aplicacion, "prefix": self._prefix, "data": data, "layout": schema, "msg": msg, "usuario": usuario, "superuser": superuser, "history": history, "aplicLabel": appLabel}
        return render(request, "YBWEB/AQdetail.html", objRender)
