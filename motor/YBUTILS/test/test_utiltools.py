from django.test import TestCase
from YBUTILS import tools


class test_utiltools(TestCase):

    def test_deepUpdate(self):
        a = {
            "a": "b",
            "c": {
                "m": "n",
                "o": {"x": {"u": "v"}, "y": "z"},
                "p": "q",
            },
            "d": "e"
        }

        b = {
            "c": {
                "o": {"x": {"u": "w"}},
                "p": "r",
            },
            "d": "f"
        }

        c = {
            "a": "b",
            "c": {
                "m": "n",
                "o": {"x": {"u": "w"}, "y": "z"},
                "p": "r",
            },
            "d": "f"
        }

        self.assertEqual(tools.deepUpdate(a, b), c)
