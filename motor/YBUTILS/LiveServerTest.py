from django.test import LiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver
from django.contrib.auth.models import User
from YBUTILS.DbRouter import fake_request


class LiveServerTest(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(LiveServerTest, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)
        try:
            cls.setUpCreateUser()
            cls.setUpLogin()
        except Exception:
            cls.tearDownClass()

    @classmethod
    def setUpCreateUser(cls):
        User.objects.create_user(username='testing', password='testing').save()

    @classmethod
    def setUpLogin(cls):
        fake_request()
        cls.selenium.get('%s%s' % ('localhost:8081', '/login/?next=/'))
        cls.selenium.find_element_by_name('username').send_keys('testing')
        cls.selenium.find_element_by_name('password').send_keys('testing')
        cls.selenium.find_element_by_name('loginSubmit').click()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(LiveServerTest, cls).tearDownClass()
