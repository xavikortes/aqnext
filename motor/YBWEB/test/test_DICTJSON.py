from django.test import TestCase
from collections import OrderedDict
from YBWEB.ctxJSON import DICTJSON


class test_DICTJSON(TestCase):

    def test_fromJSON(self):
        strJson = '{"a": "b", "c": {"d": "e"}}'
        json = OrderedDict([('a', 'b'), ('c', OrderedDict([('d', 'e')]))])
        self.assertEqual(DICTJSON.fromJSON(strJson), json)
