#!/usr/bin/env python
# -*- coding: utf-8 -*-
from YBUTILS import Factoria
import importlib


# FACTORIA DE MODULOS
class MiFactoriaModulos(Factoria.Factoria):
    """Gestiona todos los modulos"""

    def __init__(self, name):
        super(MiFactoriaModulos, self).__init__(name)

    @classmethod
    def __devuelveFalse(cls):
        return False

    def incluirYBRegistros(self, oReg):
        self.registrar('YBRegistros', oReg)

    def incluirModuloStandar(self, app, nommodulo, nomscript, prefijo=''):
        try:
            if (prefijo == "formmaster"):
                nomscript = "master" + nomscript
                prefijo = "form"

            my_module = importlib.import_module("legacy." + app + "." + nomscript)

            objeto = my_module.FormInternalObj()
            objeto._class_init()
            oObj = objeto.iface
            objeto.iface.ctx = oObj
            objeto.iface.iface = oObj
            objeto.oficial_desktopUI = self.__devuelveFalse
            if nommodulo != nomscript:
                self.registrar(prefijo + nomscript, objeto)
            self.registrar(prefijo + nommodulo, objeto)
        except SyntaxError as e:
            print("_________________________________________")
            print("Error!!! incluir modulo ", e)
            pass


# Singletons
FactoriaModulos = MiFactoriaModulos("Modulos")
