#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path
from django.apps import apps
from django.conf import settings
from YBLEGACY.Factorias import FactoriaModulos
from YBWEB.ctxJSON import DICTJSON
import importlib


class FLAux:

    __dictmodelos = dict()

    @classmethod
    def _obtenermodelo(cls, stabla):
        # Puede evaluarse apps.get_models(include_auto_created=True)
        try:
            return cls.__dictmodelos[stabla.upper()]
        except Exception:
            # SI NO SE HA REGISTRADO EXISTE OBTENEMOS DE MODELOS DE DJANGO
            for aux2 in apps.get_models(include_auto_created=True):
                try:
                    if (aux2.__name__ == stabla) or (aux2._meta.db_table == stabla):
                        cls.registrarmodelo(stabla, aux2)
                        return (aux2, None, None, None, None, None, None, None, None, None)
                except Exception:
                    pass
            raise NameError("Tabla no encontrada " + stabla)

    @classmethod
    def _obtenermodeloSimple(cls, stabla):
        return cls._obtenermodelo(stabla)[0]

    @classmethod
    def registrarmodelo(cls, stabla, model, **kwargs):
        cls.__dictmodelos[stabla.upper()] = (
            model,
            kwargs.get("bufferChanged", None),
            kwargs.get("beforeCommit", None),
            kwargs.get("afterCommit", None),
            kwargs.get("bufferCommited", None),
            kwargs.get("iniciaValoresCursor", None),
            kwargs.get("bufferChangedLabel", None),
            kwargs.get("validateCursor", None),
            kwargs.get("validateTransaction", None),
            kwargs.get("cursorAccepted", None)
        )

    @classmethod
    def registrarRest(cls):
        reg = open(path.join(settings.PROJECT_ROOT, "config/urls.json")).read()
        oReg = DICTJSON.fromJSON(reg)

        apps = {}
        apps['models'] = []
        for mod in oReg['models']:
            raiz = "models."
            if mod == "flsisppal":
                raiz = "YBSYSTEM.models."
            for modelName in oReg['models'][mod]:
                try:
                    model = importlib.import_module(raiz + mod + "." + modelName)
                    modelClass = getattr(model, modelName, None)
                # except Exception as e:
                except Exception:
                    # print(e)
                    try:
                        model = importlib.import_module(raiz + mod + ".models")
                        modelClass = getattr(model, "mtd_" + modelName, None)
                    except Exception:
                        modelClass = None
                apps['models'].append(modelClass)
        return apps

    @classmethod
    def registrarmodulos(cls):
        reg = open(path.join(settings.PROJECT_ROOT, "config/registros.json")).read()
        oReg = DICTJSON.fromJSON(reg)

        FactoriaModulos.incluirYBRegistros(oReg)

        for appName in oReg:
            raiz = "models."
            if appName == "flsisppal":
                raiz = "YBSYSTEM.models."
            # app = getattr(legacy, appName, None)
            try:
                FactoriaModulos.incluirModuloStandar(appName, appName, appName)
            except Exception:
                pass
            # modelos = getattr(app, "models", None)
            modelos = importlib.import_module(raiz + appName + ".models")
            for modelName in oReg[appName]:
                if oReg[appName][modelName] is None:
                    try:
                        FactoriaModulos.incluirModuloStandar(appName, modelName, modelName, "formmaster")
                    except SyntaxError as e:
                        print("no registra", modelName, " ", e)
                        pass
                    except Exception:
                        pass
                    try:
                        FactoriaModulos.incluirModuloStandar(appName, modelName, modelName, "formRecord")
                    except SyntaxError as e:
                        print("no registra", modelName, " ", e)
                        pass
                    except Exception:
                        pass
                else:
                    if 'formRecord' in oReg[appName][modelName]:
                        try:
                            FactoriaModulos.incluirModuloStandar(appName, modelName, oReg[appName][modelName]["formRecord"], "formRecord")
                        except Exception:
                            print("No existe el script", oReg[appName][modelName]["formRecord"], "debe incluirlo o borrarlo de los registros")
                            raise
                    if 'form' in oReg[appName][modelName]:
                        try:
                            FactoriaModulos.incluirModuloStandar(appName, modelName, oReg[appName][modelName]["form"], "form")
                        except Exception:
                            print("No existe el script", oReg[appName][modelName]["form"], "debe incluirlo o borrarlo de los registros")
                            raise

                try:
                    actionScriptModel = importlib.import_module(raiz + appName + "." + modelName)
                    actionScriptModel = getattr(actionScriptModel, modelName)
                except Exception:
                    actionScriptModel = None
                try:
                    actionScriptLegacy = FactoriaModulos.get("formRecord" + modelName).iface
                except Exception:
                    actionScriptLegacy = None

                try:
                    moduleScriptModel = importlib.import_module(raiz + appName + "." + appName + "_def").iface
                except Exception:
                    moduleScriptModel = None
                try:
                    moduleScriptLegacy = FactoriaModulos.get(appName).iface
                except Exception:
                    moduleScriptLegacy = None

                model = actionScriptModel or getattr(modelos, "mtd_" + modelName, None)
                iniciaValoresCursor = getattr(actionScriptModel, "iniciaValoresCursor", None) or getattr(actionScriptLegacy, "iniciaValoresCursor", None)
                bufferChanged = getattr(actionScriptModel, "bChCursor", None) or getattr(actionScriptLegacy, "bChCursor", None)
                bufferChangedLabel = getattr(actionScriptModel, "bChLabel", None) or getattr(actionScriptLegacy, "bChLabel", None)
                beforeCommit = getattr(moduleScriptModel, "beforeCommit_" + modelName, None) or getattr(moduleScriptLegacy, "beforeCommit_" + modelName, None)
                afterCommit = getattr(moduleScriptModel, "afterCommit_" + modelName, None) or getattr(moduleScriptLegacy, "afterCommit_" + modelName, None)
                bufferCommited = getattr(moduleScriptModel, "bufferCommited_" + modelName, None) or getattr(moduleScriptLegacy, "bufferCommited_" + modelName, None)
                validateCursor = getattr(actionScriptModel, "validateCursor", None) or getattr(actionScriptLegacy, "validateCursor", None)
                validateTransaction = getattr(actionScriptModel, "validateTransaction", None) or getattr(actionScriptLegacy, "validateTransaction", None)
                cursorAccepted = getattr(actionScriptModel, "cursorAccepted", None)
                # print(model, bufferCommited)
                FLAux.registrarmodelo(
                    modelName,
                    model,
                    iniciaValoresCursor=iniciaValoresCursor,
                    bufferChanged=bufferChanged,
                    bufferChangedLabel=bufferChangedLabel,
                    beforeCommit=beforeCommit,
                    afterCommit=afterCommit,
                    bufferCommited=bufferCommited,
                    validateCursor=validateCursor,
                    validateTransaction=validateTransaction,
                    cursorAccepted=cursorAccepted
                )
